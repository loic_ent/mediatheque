class ADMINISTRATEUR
inherit UTILISATEUR redefine to_string end

creation {ANY}
	administrateur

feature {ANY}

feature {ANY}
    administrateur (id: STRING; n: STRING; p: STRING) is
        do
            identifiant := id
            nom := n
            prenom := p
				create liste_medias_empruntes.make
        end
        
    to_string: STRING is
        do
            Result := "%NAdministrateur : "+"%N" +"Identifiant = " + identifiant + "%NNom = " + nom + "%NPrenom = " + prenom + "%N"
        end
   
end
