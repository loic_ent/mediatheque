class MEDIATHEQUE
--
-- La classe mediatheque
--
--
	
creation {ANY}
	make

feature {}
	fichier_medias : TEXT_FILE_READ -- fichier qui contient les  medias
	fichier_utilisateurs : TEXT_FILE_READ -- fichier qui contient les utilisateurs
	liste_media: LINKED_LIST[MEDIA] --liste qui contient les medias
	liste_utilisateur: LINKED_LIST[UTILISATEUR] --liste qui contient les utilisateurs
	last_id : INTEGER
	utilisateur : UTILISATEUR
	connecte: BOOLEAN
feature {ANY}
	make is
		local
			choix : INTEGER --choix de l'utilisateur dans le menu principal
			i : INTEGER
			id : STRING
			res, quit,deconnexion : BOOLEAN

		do

			-------------------------------
         -- INITIALISATION DES LISTES --
         -------------------------------

			--creation des listes
			create liste_media.make;
			create liste_utilisateur.make;
			create fichier_utilisateurs.connect_to("files/utilisateurs.txt");
			create fichier_medias.connect_to("files/medias.txt");

			-- créer toutes les instances de médias
			init_liste_media
			-- créer toutes les instances d'utilisateur
			init_liste_utilisateur
			
			
	
			-----------------------------------
         -- AFFICHAGE DE L'INITIALISATION --
         -----------------------------------
					
			io.put_string("%N===============================")
			io.put_string("%N|BIENVENUE DANS LA MEDIATHEQUE|")
			io.put_string("%N===============================")
			io.flush


			from
				quit:=False
			until
				quit=True
			loop --principal
				io.put_string("%NConnexion")
				io.put_string("%NEntrez votre identifiant (entrez 'q' pour quitter) : ")
				
				id := ""
				res := False
				io.flush
				--fixbug du readline
				if(connecte = True) then
					io.read_line
					connecte := False
				end
				io.read_line
				id.copy(io.last_string)
				io.disconnect
				if(id.is_equal("q")) then
					quit := True
				else
					from
						i := 1    
					until
						i = liste_utilisateur.count+1 or res = True or quit = True
					loop
						if (liste_utilisateur.item(i).get_id.is_equal(id)) then
							res := True
							connecte := True
							utilisateur := liste_utilisateur.item(i)
						end
						i := i+1
					end
					if (res = False) then
						io.put_string("L'utilisateur n'existe pas !")
					end
				end


				
			-- on est connecté : on affiche le bon menu
				if(connecte = True) then
					if ({ADMINISTRATEUR} ?:= liste_utilisateur.item(i-1)) then
						-- Menu Administrateur
						io.put_string("%N=========================================")
						io.put_string("%N|BIENVENUE DANS LE MENU ADMINISTRATEUR|")
						io.put_string("%N=========================================")
						io.put_string("%N%NVous êtes identifié en tant que " + liste_utilisateur.item(i-1).get_prenom + " " + liste_utilisateur.item(i-1).get_nom)
						io.flush
						from
							deconnexion := False
						until
							deconnexion = True
						loop
							io.put_string("%N%N    #######    GERER MEDIAS    #######%N")
							io.put_string("Tapez 1 pour ajouter un media %N")
							io.flush
							io.put_string("Tapez 2 pour afficher tous les medias %N")
							io.flush
							io.put_string("Tapez 3 pour supprimer un media %N")
							io.flush
							io.put_string("Tapez 4 pour rechercher des medias %N")
							io.flush
							io.put_string("Tapez 5 pour ajouter des exemplaires d'un media %N")
							io.flush

							io.put_string("%N%N     #######   GERER UTILISATEUR   #######%N%N")
							
							io.put_string("Tapez 6 pour ajouter un utilisateur %N")
							io.flush
							io.put_string("Tapez 7 pour afficher tous les utilisateurs %N")
							io.flush
							io.put_string("Tapez 8 pour modifier un utilisateur %N")
							io.flush
							io.put_string("Tapez 9 pour supprimer un utilisateur %N%N%N")
							io.flush
							io.put_string("Tapez 0 pour quitter%N")
							io.flush

							--demande du choix à l'utilisateur
							io.read_integer
							choix := io.last_integer -- l'utilisateur fait son choix
							inspect choix
							when 1 then ajouter_media
							when 2 then afficher_medias
							when 3 then supprimer_media
							when 4 then rechercher_media
							when 5 then ajouter_exemplaire
							when 6 then ajouter_utilisateur
							when 7 then afficher_utilisateurs
							when 8 then modifier_utilisateur
							when 9 then supprimer_utilisateur
							when 0 then deconnexion := True
							end
						end
		    
					else
						-- Menu Client
						io.put_string("%N=========================================")
						io.put_string("%N|BIENVENUE DANS LE MENU CLIENT|")
						io.put_string("%N=========================================")
						io.put_string("%NVous êtes identifié en tant que " + liste_utilisateur.item(i-1).get_prenom + " " + liste_utilisateur.item(i-1).get_nom)
						io.flush
						from
							deconnexion := False
						until
							deconnexion = True
						loop
							io.put_string("%NTapez 1 rechercher des medias %N")
							io.flush
							io.put_string("Tapez 2 pour consulter un media %N")
							io.flush
							io.put_string("Tapez 3 pour emprunter un media %N")
							io.flush
							io.put_string("Tapez 4 pour rendre un media %N")
							io.flush
							io.put_string("Tapez 5 pour lister les medias empruntés %N")
							io.flush
							io.put_string("Tapez 0 pour quitter%N")
							io.flush

							--demande du choix à l'utilisateur
							io.read_integer
							choix := io.last_integer -- l'utilisateur fait son choix
							inspect choix
							when 1 then rechercher_media
							when 2 then consulter_media
							when 3 then emprunter_media
							when 4 then retourner_media
							when 5 then lister_medias_empruntes
							when 0 then deconnexion := True
							end
						end
					end --END IF
				end-- end if connecte
			end --END loop Principal
			fichier_medias.disconnect
			fichier_utilisateurs.disconnect
		end

			---------------------------------------------
			-- ajouter un utilisateur manuellement --
			---------------------------------------------
	ajouter_utilisateur is
		local
			identifiant, nom, prenom : STRING	
			choix : INTEGER	
			administrateur : ADMINISTRATEUR
			client : CLIENT
		do
			prenom := ""
			nom := ""
			identifiant := ""
		    
			-- get prenom
			io.put_string(" %NSaisir le prenom de l'utilisateur : %N")
			io.flush
			io.read_line
			io.read_line
			prenom.copy(io.last_string) -- l'utilisateur fait son choix
			io.disconnect
			-- get nom
			io.put_string(" %NSaisir le nom de l'utilisateur : %N")
			io.flush
			io.read_line
			nom.copy(io.last_string) -- l'utilisateur fait son choix
			io.disconnect
			-- get identifiant
			io.put_string(" %NSaisir l'identifiant de l'utilisateur %N")
			io.flush
			io.read_line
			identifiant.copy(io.last_string) -- l'utilisateur fait son choix
			io.disconnect
			io.put_string("L'utilisateur est un administrateur")
			io.put_string(" %N 1- Oui %N")
			io.put_string(" %N 2- Non %N")
			io.flush
			--demande du choix à l'utilisateur
			io.read_integer
			choix := io.last_integer -- l'utilisateur fait son choix
			io.disconnect
			if (choix.is_equal(1)) then
				create administrateur.administrateur(identifiant, nom, prenom)
				liste_utilisateur.add_first(administrateur)
			else
		        
				create client.client(identifiant, nom, prenom)
				liste_utilisateur.add_first(client)
			end
		end
		
	afficher_utilisateurs is
		local
			i : INTEGER
		do
			from
            i := 1    
			until
				i = liste_utilisateur.count+1
			loop
				io.put_string((liste_utilisateur.item(i)).to_string)
				i := i + 1
				io.put_string("===============================")
			end
	
		end


			---------------------------------------------
			-- afficher tous les medias du fichier à la liste --
			---------------------------------------------

	afficher_medias is
		local
			i : INTEGER
		do
 			from
            i := 1    
			until
				i = liste_media.count+1
			loop
				io.put_string((liste_media.item(i)).to_string)
				i := i + 1
				io.put_string("=======================================")
			end
	
		end


			---------------------------------------------
			-- ajouter les utilisateurs du fichier à la liste --
			---------------------------------------------
	init_liste_utilisateur is
		local
			index1, index2 : INTEGER
			buffer, nom, prenom, identifiant : STRING
			administrateur : ADMINISTRATEUR
			client : CLIENT
			
		do
			
			-- si il peut lire
			if(fichier_utilisateurs.can_read_line) then 
				from
					buffer := ""
				until 
					fichier_utilisateurs.end_of_input
				loop
					buffer := ""
					index1 := 1
					index2 := 1
					fichier_utilisateurs.read_line_in(buffer)
					--extraction du prenom, index_of du '<'
					if (not index1.is_equal(0)) then
						index1 := buffer.index_of('<',index1+1)
						index2 := buffer.index_of('>',index1+1)
						nom := buffer.substring(index1+1, (index2-1))
						--extraction du nom, index_of du '<'
						index1 := buffer.index_of('<',index1+1)
						index2 := buffer.index_of('>',index1+1)
						prenom := buffer.substring(index1+1, (index2-1))
						--extraction de l'identifiant, index_of du '<'
						index1 := buffer.index_of('<',index1+1)
						index2 := buffer.index_of('>',index1+1)
						identifiant := buffer.substring(index1+1, (index2-1))
						--extraction de admin, index_of du '<'
						index1 := buffer.index_of('<',index1+1)
						index2 := buffer.index_of('>',index1+1)
						if (buffer.substring(index1+1, (index2-1)).is_equal("OUI")) then
							create administrateur.administrateur(identifiant, nom, prenom)
							liste_utilisateur.add_first(administrateur)
						else
							create client.client(identifiant, nom, prenom)
							liste_utilisateur.add_first(client)
						end

					end	
				end
				
			else
				io.put_string("Impossible de lire le fichier")
			end
		end
	


			----------------------
			-- Ajouter un media --
			----------------------
	ajouter_media is
		local
			titre, auteur, real, real_tmp : STRING
			nombre,i,nb_real, id : INTEGER			
			livre : LIVRE
			dvd : DVD
			choix : INTEGER
			
		do
			real := ""
			real_tmp := ""
			titre := ""
			auteur :=""
			io.put_string("%N1- Ajouter un livre")
			io.put_string("%N2- Ajouter un DVD %N")
			io.flush
			--demande du choix à l'utilisateur
			io.read_integer
			choix := io.last_integer -- l'utilisateur fait son choix
			id := last_id+1
			last_id := last_id+1
			if(choix.is_equal(1)) then
				-- get titre
				io.put_string("%NSaisir le titre du livre : %N")
				io.flush
				io.read_line
				io.read_line
				titre.copy(io.last_string) -- l'utilisateur fait son choix
				io.disconnect
				-- get auteur
				io.put_string("%NSaisir l'auteur du livre : %N")
				io.flush
				io.read_line
				auteur.copy(io.last_string) -- l'utilisateur fait son choix
				io.disconnect
				-- get nombre
				io.put_string("%NSaisir le nombre d'exemplaire : %N")
				io.flush
				io.read_integer
				nombre := io.last_integer -- l'utilisateur fait son choix

				-- ajouter le livre a media
				create livre.livre(id, titre, nombre, auteur)
				liste_media.add_first(livre)

			else -- if add DVD
				-- /!\ create DVD before to be able to add realisateur or Acteur

				-- id,titre,nb,annee,type
				create dvd.dvd(id, "", 1, 0, "")
				
				-- get titre
				io.put_string("%NSaisir le titre du DVD : %N")
				io.flush
				io.read_line
				io.read_line
				dvd.set_titre(io.last_string) 
				-- get nombre
				io.put_string("%NSaisir le nombre d'exemplaire dispo : %N")
				io.flush
				io.read_integer
				dvd.set_nombre(io.last_integer)
				-- get annee
				io.put_string("%NSaisir l'annee du film : %N")
				io.flush
				io.read_integer
				dvd.set_annee(io.last_integer) -- l'utilisateur fait son choix
				-- get type (coffret)
				io.put_string("%NSaisir le type (coffret ...) : %N")
				io.flush
				io.read_line
				io.read_line
				dvd.set_type(io.last_string)
				-- get realisateur
				io.put_string("%NSaisir le nombre de realisateur : %N")
				io.flush
				io.read_integer
				nb_real:=io.last_integer

				from
					i := 0
					io.read_line
				until
					i = nb_real
				loop
					io.put_string("%NSaisir un realisateur : %N")
					io.flush
					io.read_line
					real.copy(io.last_string)
					dvd.ajouter_realisateur(real)
					io.flush
					io.disconnect
					i:=i+1
				end
				-- get acteur
				io.put_string("%NSaisir le nombre d'acteur : %N")
				io.flush
				io.read_integer
				nb_real:=io.last_integer
				from
					i := 0
					io.read_line
				until
					i = nb_real
				loop
					io.put_string("%NSaisir un acteur : %N")
					io.flush
					io.read_line
					io.disconnect
					dvd.ajouter_acteur(io.last_string)
					i:=i+1
				end

				liste_media.add_first(dvd)
			end
		end


			-------------------------
			-- Rechercher un media --
			-------------------------

	rechercher_media is
		local
			titre,titre_tmp, saisie : STRING
			i,id : INTEGER
		do
			io.put_string("%NSaisir un titre (mots clés) : %N")
			io.flush
			io.read_line
			io.read_line
			saisie := io.last_string
			saisie.to_lower
			io.disconnect

			io.put_string("%N%N%N ================== %N Résultats trouvés %N ==================%N")
			-- search in all media
			from
            i := 1    
			until
				i = liste_media.count+1
			loop
				titre := liste_media.item(i).get_titre
				titre_tmp:=""
				titre_tmp.copy(titre)
				titre_tmp.to_lower

				id := liste_media.item(i).get_id
				io.flush
				if(titre_tmp.has_substring(saisie)) then
					io.put_integer(id)
					io.put_string(" - ")
					io.put_string(titre)
					io.flush
					io.put_string("%N")
				end
				i := i + 1
			end
		end --END recherche_media


			------------------------
			-- Consulter un media --
			------------------------
	
	consulter_media is
		local
			saisie,i, id : INTEGER
		do
			io.put_string("%NSaisir l'identifiant d'un média : %N")
			io.flush
			io.read_integer
			
			saisie := io.last_integer
			
			io.disconnect

			from
            i := 0    
			until
				i = liste_media.count+1 or i = -1
			loop
				i:=i+1
				id := liste_media.item(i).get_id

				if(id.is_equal(saisie)) then
					io.put_string("%N======== "+ liste_media.item(i).get_titre + " ========%N")
					io.put_string(liste_media.item(i).to_string + "%N")
					i := -1
				end

				
			end --end loop
		end --END consulter_media


			------------------------
			-- Emprunter un media --
			------------------------

	emprunter_media is
		local
			saisie,i, id,nombre : INTEGER
		do
			io.put_string("%NSaisir l'identifiant du média a emprunter : %N")
			io.read_integer
			saisie := io.last_integer 
			io.flush
			
			from
            i := 0    
			until
				i = liste_media.count+1 or i = -1
			loop
				i := i+1
				id := liste_media.item(i).get_id
				nombre := liste_media.item(i).get_nombre

				if(id.is_equal(saisie)) then -- media non disponible
					if(nombre.is_equal(0)) then
						io.put_string("%N%N%N%N    == "+ liste_media.item(i).get_titre + " n'est plus disponible %N%N%N")
						i := -1

					else -- emprunter media
						-- decremente le nombre de media current
						liste_media.item(i).set_nombre(nombre-1)
						utilisateur.ajouter_emprunt(liste_media.item(i))
						io.put_string("%N%N%N   ==  Vous avez emprunté  "+ liste_media.item(i).get_titre + " %N%N%N%N")
						i := -1
					end
				end
				
			end --end loop
		
		end --END emprunter_media
	


			--------------------------
			-- Ajouter un exmplaire --
			--------------------------
	ajouter_exemplaire is
		local
			saisie,nombre_add,i, id,nombre : INTEGER
			trouve : BOOLEAN
		do
			io.put_string("%NSaisir l'identifiant du média du nouvel exemplaire : %N")
			io.read_integer
			saisie := io.last_integer 
			io.flush
			
			io.put_string("%NSaisir le nombre d'exemplaire(s) à ajouter : %N")
			io.read_integer
			nombre_add := io.last_integer 
			io.flush
			
			
			trouve := False
			from
            i := 1
			until
				i = liste_media.count+1 or trouve = True
			loop
				id := liste_media.item(i).get_id
				nombre := liste_media.item(i).get_nombre

				if(id.is_equal(saisie)) then -- media non disponible
					liste_media.item(i).set_nombre(nombre+nombre_add)
					io.put_string("%N%N%N   ==  Vous avez ajouté "+nombre_add.to_string+" exemplaire(s) de  "+ liste_media.item(i).get_titre + " %N%N%N%N")
					trouve := True
				end
				i := i+1
			end  --end loop
		end --END emprunter_media
	

	

			---------------------
			-- Retourner_media --
			---------------------
	retourner_media is
		local
			saisie,i,y, id,nombre : INTEGER
			possede : BOOLEAN
		do
			io.put_string("%NSaisir l'identifiant du média a retourner : %N")
			io.read_integer
			saisie := io.last_integer 
			io.flush

			possede := False
			--verifie que l'on possede le media
			from
            i := 1
			until
				i = utilisateur.get_liste_emprunts.count+1 or possede = True
			loop
				--si on le possede
				if(utilisateur.get_liste_emprunts.item(i).get_id.is_equal(saisie)) then
					if(utilisateur.get_liste_emprunts.count>0) then
						utilisateur.get_liste_emprunts.remove(i)
					end
					possede := True --recherche dans la mediatheque le media + incremente
				end
				i:=i+1
			end -- END from

			
			-- si on possede pas le media
			if(not possede) then
				io.put_string("%N%N  Vous n'avez pas emprunté ce média %N%N")
			else -- si on le possede
				-- on increment dans la mediatheque
				from
					y := 0
				until
					y = liste_media.count+1 or y = -1
				loop
					y := y+1
					id := liste_media.item(y).get_id
					nombre := liste_media.item(y).get_nombre
					if(id.is_equal(saisie)) then
						io.put_string("lala")
						liste_media.item(y).set_nombre(nombre+1)
						io.put_string("%N%N%N   ==  Vous avez retourné "+ liste_media.item(y).get_titre + " %N Merci %N%N%N")
						y := -1
					end
				end --END FROM
			end --if
			end --END retourner_media

	
			---------------------------------------------
			-- Ajouter les media du fichier à la liste --
			---------------------------------------------
			init_liste_media is
		local
			index1, index2, nombre, annee, id : INTEGER
			buffer, type_media, titre, auteur, tmp : STRING
			livre: LIVRE
			dvd: DVD
		do
			-- si il peut lire
			if(fichier_medias.can_read_line) then 
				from
					buffer := ""
				until 
					fichier_medias.end_of_input
				loop
					buffer := ""
					index1 := 1
					index2 := 1
					nombre := 1
					-- increment id
					id := last_id
					last_id := last_id+1
					fichier_medias.read_line_in(buffer)
					--extraction du type, index_of du ';'
					index1 := buffer.index_of(';',1)
					if (not index1.is_equal(0)) then
						type_media := buffer.substring(1, (index1-2))
						--extraction du titre, index_of du '<'
						index1 := buffer.index_of('<',index1+1)
						index2 := buffer.index_of('>',index1+1)
						titre := buffer.substring(index1+1, (index2-1))

						-- if livre ou DVD
						if (type_media.is_equal("Livre")) then
							--extraction de lauteur, index_of du '>'
							index1 := buffer.index_of('<',index2+1)
							index2 := buffer.index_of('>',index1+1)
							auteur := buffer.substring(index1+1, (index2-1))
				        
							index1 := buffer.index_of('<',index2+1)
							index2 := buffer.index_of('>',index1+1)
							tmp := buffer.substring(index1+1, (index2-1))
							if (tmp.is_integer) then
				            nombre := tmp.to_integer
							else
								nombre:= 1
							end
							create livre.livre(id, titre, nombre, auteur)
							liste_media.add_first(livre)
							--io.put_string(livre.to_string)
							
							-- si c'est un DVD
						elseif (type_media.is_equal("DVD")) then
							index1 := buffer.index_of('<',index2+1)
							index2 := buffer.index_of('>',index1+1)
							tmp := (buffer.substring(index1+1, (index2-1)))
							-- get annee
							if (tmp.is_integer) then
				            annee := tmp.to_integer
							end
 				        
							create dvd.dvd(id, titre, 1, annee, " ") 
							from
				            index1 := buffer.index_of(';',index2)
							until
				            index1 = 0
							loop
				            index2 := buffer.index_of('>',index1+1)
				            tmp := buffer.substring(index1+2, (index2))
								-- get and add all realisateur
								if (tmp.has_prefix("Realisateur<") = True) then
									tmp.replace_all('>',' ')
									tmp.remove_prefix("Realisateur<")
									dvd.ajouter_realisateur(tmp)
								end
								-- get and add all Actors
				            if (tmp.has_prefix("Acteur<") = True) then
									tmp.replace_all('>',' ')
									tmp.remove_prefix("Acteur<")
									dvd.ajouter_acteur(tmp)
				                
				            end
				            -- get type
				            if (tmp.has_prefix("Type<") = True) then
									tmp.replace_all('>',' ')
									tmp.remove_prefix("Type<")
									dvd.set_type(tmp)
				            end

								--get nombre
				            if (tmp.has_prefix("Nombre<") = True) then
									tmp.replace_all('>',' ')
									tmp.remove_prefix("Nombre<")
									if (tmp.is_integer) then
										dvd.set_nombre(tmp.to_integer)
									end
				            end
								index1 := buffer.index_of(';',index2)
							end
							liste_media.add_first(dvd)
						end	
					end	
				end

				-- if can't read
			else
				io.put_string("Impossible de lire le fichier")
			end
			
		end --end "init_liste_media
	
		
	

	recherche_utilisateur is
		local
			i : INTEGER
			id : STRING
			res : BOOLEAN
		do
			id := ""
			res := False
			io.put_string("%NSaisir l'identifiant de l'utilisateur : %N")
			io.flush
			io.read_line
			io.read_line
			id.copy(io.last_string) -- l'utilisateur fait son choix
			io.disconnect
			from
            i := 1    
			until
				i = liste_utilisateur.count or res = True
			loop
				if (liste_utilisateur.item(i).get_id.is_equal(id)) then
					io.put_string("L'utilisateur existe !")
					res := True
				end
				i := i+1
			end
			if (res = False) then
				io.put_string("L'utilisateur n'existe pas !")
			end
		end -- END recherche_utilisateur
		
	modifier_utilisateur is
		local
			i, choix : INTEGER
			id, prenom, nom : STRING
			res : BOOLEAN
		do
			id := ""
			nom := ""
			prenom := ""
			res := False
			io.put_string("%NSaisir l'identifiant de l'utilisateur : %N")
			io.flush
			io.read_line
			io.read_line
			id.copy(io.last_string) -- l'utilisateur fait son choix
			io.disconnect
			from
				i := 1    
			until
				i = liste_utilisateur.count or res = True
			loop
				if (liste_utilisateur.item(i).get_id.is_equal(id)) then
					res := True
				end
				i := i+1
			end
			if (res = False) then
				io.put_string("L'utilisateur n'existe pas !")
			else
				i := i-1
				io.put_string("%NQue voulez-vous modifier ?%N")
				io.put_string("%N1 - Nom%N")
				io.put_string("%N2 - Prenom%N")
				io.put_string("%N3 - Nom et Prenom%N")
				io.flush
				io.read_integer
				choix := io.last_integer
				io.disconnect
				inspect choix
				when 1 then
					io.put_string("%NEntrez le nouveau nom : %N")
					io.flush
					io.read_line
					io.read_line
					nom.copy(io.last_string)
					liste_utilisateur.item(i).set_nom(nom)
					io.disconnect
				when 2 then
					io.put_string("%NEntrez le nouveau prenom : %N")
					io.flush
					io.read_line
					io.read_line
					prenom.copy(io.last_string)
					liste_utilisateur.item(i).set_prenom(prenom)
				when 3 then
					io.put_string("%NEntrez le nouveau nom : %N")
					io.flush
					io.read_line
					io.read_line
					nom.copy(io.last_string)
					liste_utilisateur.item(i).set_nom(nom)
					io.put_string("%NEntrez le nouveau prenom : %N")
					io.flush
					io.read_line
					prenom.copy(io.last_string)
					liste_utilisateur.item(i).set_prenom(prenom)
				end
			end
		end -- END modifier_utilisateur
		
	supprimer_utilisateur is
		local
			i : INTEGER
			id : STRING
			res : BOOLEAN
		do
			id := ""
			res := False
			io.put_string("%NSaisir l'identifiant de l'utilisateur : %N")
			io.flush
			io.read_line
			io.read_line
			id.copy(io.last_string) -- l'utilisateur fait son choix
			io.disconnect
			from
            i := 1    
			until
				i = liste_utilisateur.count+1 or res = True
			loop
				if (liste_utilisateur.item(i).get_id.is_equal(id)) then
					liste_utilisateur.remove(i)
					res := True
				end
				i := i+1
			end
			if (res = False) then
				io.put_string("L'utilisateur n'existe pas !")
			end
		end --END supprimer_utilisateur
		
	supprimer_media is
		local
			i : INTEGER
			id : INTEGER
			res : BOOLEAN
		do
			res := False
			io.put_string("%NSaisir l'identifiant du media : %N")
			io.flush
			io.read_integer
			id := io.last_integer -- l'utilisateur fait son choix
			io.disconnect
			from
            i := 1    
			until
				i = liste_media.count+1 or res = True
			loop
				if (liste_media.item(i).get_id.is_equal(id)) then
					liste_media.remove(i)
					res := True
				end
				i := i+1
			end
			if (res = False) then
				io.put_string("Le media n'existe pas !")
			end
		end -- END supprimer_media

	
	lister_medias_empruntes is
		do
			io.put_string(utilisateur.lister_emprunt)			
		end -- END lister_medias_empruntes

end -- class MEDIATHEQUE

-- class MEDIATHEQUE
-- {TYPE} ?:= exp



