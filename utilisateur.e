class UTILISATEUR
	
creation {ANY}
	utilisateur

feature {ANY}
	identifiant : STRING	-- Identifiant de l'utilisateur
	nom: STRING       -- Nom de l'utilisateur
	prenom: STRING		-- Prenom de l'utilisateur
	liste_medias_empruntes : LINKED_LIST[MEDIA] --liste qui contient les medias empruntes

	
feature {ANY}
	utilisateur (id: STRING; n: STRING; p: STRING) is
		do
			identifiant := id
			nom := n
			prenom := p
			--creation des listes
			create liste_medias_empruntes.make
		end


			-- ajoute un emprunt à la liste
	ajouter_emprunt(e:MEDIA) is
		do
			liste_medias_empruntes.add_first(e)
		end

			-- supprime un emprunt de la liste
	retourner_emprunt(e:MEDIA) is
		local
			index : INTEGER
		do
			index := liste_medias_empruntes.index_of(e,0)
			liste_medias_empruntes.remove(index)
		end


			-- afficher tout les media empruntés par un utilisateur
	lister_emprunt : STRING is
		local
			i : INTEGER
			liste_emprunt : STRING
		do
			liste_emprunt := ""
			if(liste_medias_empruntes.count.is_equal(0)) then
				Result := "%N%N   Aucun média emprunté %N%N"
			else
				from
					i:=1
				until
					i>liste_medias_empruntes.count
				loop
					liste_emprunt.copy(liste_emprunt+"%N "+liste_medias_empruntes.item(i).to_string)
					i:=i+1
				end
				Result := "===========%N%N Vous avez emprunté : %N%N" +liste_emprunt + "%N ==============="
			end
		end
    
		-- Retourne le nom de l'utilisateur
		get_nom: STRING is
		do
			Result := nom
		end
    
			-- Retourne le prenom de l'utilisateur
	get_prenom: STRING is
		do
			Result := prenom
		end
    
	set_prenom (p:STRING) is
		do
			prenom := p
		end

			-- Retourne l id du media
	get_id: STRING is
		do
			Result := identifiant
		end

			-- Retourne la liste des emprunts
	get_liste_emprunts : LINKED_LIST[MEDIA] is
		do
			Result := liste_medias_empruntes
		end

			  
	set_nom (n:STRING) is
		do
			nom := n
		end
        
	to_string: STRING is
		do
			Result := "Nom = " + nom + "%NPrenom = " + prenom + "%N"
		end    
   
   
end
