class MEDIA
	
creation {ANY}
	media

feature {ANY}
    id : INTEGER
            -- Identifiant du media
	titre: STRING
	        -- Titre du media
	nombre: INTEGER
	        -- Nombre de media disponible

feature {ANY}
    media (ident: INTEGER; titre_media: STRING; nb: INTEGER) is
        do
            id := ident
            titre := titre_media
            nombre := nb
        end
    
    -- Retourne le titre du media
    get_titre: STRING is
        do
            Result := titre
        end
    
    -- Retourne le nombre d'exemplaire du media
    get_nombre: INTEGER is
        do
            Result := nombre
        end

    -- Retourne l id du media
    get_id: INTEGER is
        do
            Result := id
        end

		  
    set_nombre (nb: INTEGER) is
        do
            nombre := nb
        end
    
    -- Retourne une string avec les attributs du media
    to_string: STRING is
        do
            Result := "ID = " +id.to_string + "%NTitre = " + titre + "%NNombre = " + nombre.to_string + "%N"
        end
end
