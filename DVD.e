class DVD
inherit MEDIA redefine to_string end
	
creation {ANY}
	dvd

feature {ANY}
	annee: INTEGER
	type: STRING
	realisateur : ARRAY[STRING]
	acteur : ARRAY[STRING]
	
	 
feature {ANY}
	dvd (ident: INTEGER; titre_media: STRING; nb: INTEGER; an: INTEGER; t: STRING) is
		do
			id := ident
			titre := titre_media
			nombre := nb   
			annee := an
			type := t
			create realisateur.with_capacity(0,1)
			create acteur.with_capacity(0,1)
		end

	to_string: STRING is
		local
			acteur_str : STRING
			realisateur_str : STRING
			i : INTEGER
		do
			-- create a string content all realisateurs
			realisateur_str :=""
			acteur_str := ""
			if(not realisateur.count.is_equal(0)) then
				realisateur_str.copy(realisateur.item(1))
				from
					i:=2
				until
					i>realisateur.count
				loop
					realisateur_str.copy(realisateur_str+"; "+realisateur.item(i))
					i:=i+1
				end
			else
				realisateur_str.copy("aucun")
			end -- endif
			-- create a string content all actors
			if(not acteur.count.is_equal(0)) then
				acteur_str.copy(acteur.item(1))
				from
					i:=2
				until
					i>acteur.count
				loop
					acteur_str.copy(acteur_str+"; "+acteur.item(i))
					i:=i+1
				end
			else
				acteur_str.copy("aucun")
			end -- end if
				
			Result :="%NID = " +id.to_string + "%NTitre = " + titre + "%Nannee = " + annee.to_string + "%NNombre = " + nombre.to_string + "%NRealisateur = " + realisateur_str + "%NType = " + type +"%NActeurs = " + acteur_str + "%N"
		end
    


	ajouter_realisateur (real: STRING) is
		local
		    tmp: STRING
		do
		    tmp := ""
		    tmp.copy(real)
			realisateur.add_last(tmp)
		end

	ajouter_acteur (act: STRING) is
		local
		    tmp: STRING
		do
		    tmp := ""
		    tmp.copy(act)
			acteur.add_last(tmp)
		end
      
	set_type (t: STRING) is
		do
			type.copy(t)
		end
		  
	set_titre (t: STRING) is
		do
			titre.copy(t)
		end
	
	set_annee (a: INTEGER) is
		do
			annee := a
		end
		  
end
