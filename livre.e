class LIVRE
inherit MEDIA redefine to_string end
	
creation {ANY}
	livre

feature {ANY}
    auteur: STRING

feature {ANY}
    livre (ident: INTEGER; titre_media: STRING; nb: INTEGER; aut: STRING) is
        do
            id := ident
            titre := titre_media
            nombre := nb
            auteur := aut
        end
        
    to_string: STRING is
    do
        Result := "%NID = " +id.to_string + "%NTitre = " + titre + "%NNombre = " + nombre.to_string + "%NAuteur = " + auteur + "%N"
    end
    
end
