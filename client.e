class CLIENT
inherit UTILISATEUR redefine to_string end
	
creation {ANY}
	client

feature {ANY}

feature {ANY}
    client (id: STRING; n: STRING; p: STRING) is
        do
            identifiant := id
            nom := n
            prenom := p
				create liste_medias_empruntes.make
        end
    
    to_string: STRING is
        do
            Result := "%NClient : "+"%N" +"Identifiant = " + identifiant + "%NNom = " + nom + "%NPrenom = " + prenom + "%N"
        end
   
end
